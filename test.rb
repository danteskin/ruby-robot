require_relative 'script.rb'
require 'rspec'
# require 'watir'
# require 'watirspec/rake_tasks'
# WatirSpec::RakeTasks.new

describe 'MyTargetPadsCreator' do

  pads_to_create = [
    {
      description: 'news_pad_with_a_few_unit',
      app_link: 'https://itunes.apple.com/us/app/angry-birds/id343200656?mt=8',
      ad_units: [
        {
          name: 'pad name 1_1'        
        },
        {
          name: 'pad name 2_2'        
        }
      ]
    },
    {
      description: 'news_pad_with_a_few_unit 2',
      app_link: 'https://itunes.apple.com/us/app/angry-birds/id343200656?mt=8',
      ad_units: [
        {
          name: 'pad name 2_1'        
        },
        {
          name: 'pad name 2_2'        
        }
      ]
    }

  ]

  let(:session) { MyTargetPadsCreator.new(
    url: "https://target-sandbox.my.com",
    login: "buck2192@gmail.com",
    password: "123qweasd",
    pads_to_create: pads_to_create
  )}
  let(:browser) { session.browser }

  describe '#visit_page' do
    it "should return url https://target-sandbox.my.com" do
      browser.goto session.url
      expect(browser.url).to eq('https://target-sandbox.my.com/')
    end
  end

  describe '#authorize' do
    
    it "should return main url /pad_groups/" do
      
      browser.goto session.url
      session.authorize
      expect(browser.url).to eq("#{session.url}/pad_groups/")
    end
  end

  describe '#create pad_group and ad units' do
    let(:create_pad) do
      {
        description: 'news_pad_with_a_few_unit',
        app_link: 'https://itunes.apple.com/us/app/angry-birds/id343200656?mt=8',
        ad_units: [
          {
            name: 'testingp ad name 1_1'        
          },
          {
            name: 'testing pad name 2_2'        
          }
        ]
      }
    end

    before { 
      browser.goto session.url
      session.authorize 
    }
    describe "#create_pad_with_first_ad_unit" do
      it "should return id of new group pad" do

        session.create_pad_with_first_ad_unit(
          description: create_pad[:description],
          app_link: create_pad[:app_link],
          ad_units: create_pad[:ad_units]
        )
        browser.goto("#{@url}/api/v2/pad_groups.json")
        response_pad_groups = browser.text
        pad_groups_json = JSON.parse(response_pad_groups)

        expect(pad_groups_json.last["description"] ).to eq(create_pad[:description])
      end
    end
    
    describe "#create_additional_ad_units" do
      it "should return ad_unit id " do
        session.create_additional_ad_units(ad_units: create_pad[:ad_units])

        browser.goto("#{session.url}/api/v2/pad_groups.json")
        response_pad_groups = browser.text
        pad_groups_json = JSON.parse(response_pad_groups)
        create_pad_id = pad_groups_json.last["id"] 

        browser.goto("#{session.url}/api/v2/pad_groups/#{create_pad_id}.json")
        response_pad_with_id = browser.text
        pad_with_id_json = JSON.parse(response_pad_with_id)
        unit_id = pad_with_id_json['pads'].last['id']
        pad_with_id_json['pads'].last['description']
        
        expect(pad_with_id_json['pads'].last['description'] ).to eq(create_pad[:ad_units].last[:name])
      end
    end
    


  end


  
end






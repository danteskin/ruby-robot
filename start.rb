require_relative 'script.rb'

pads_to_create = [
  {
    description: 'news_pad_with_a_few_unit',
    app_link: 'https://itunes.apple.com/us/app/angry-birds/id343200656?mt=8',
    ad_units: [
      {
        name: 'pad name 1_1'        
      },
      {
        name: 'pad name 2_2'        
      }
    ]
  },
  {
    description: 'news_pad_with_a_few_unit 2',
    app_link: 'https://itunes.apple.com/us/app/angry-birds/id343200656?mt=8',
    ad_units: [
      {
        name: 'pad name 2_1'        
      },
      {
        name: 'pad name 2_2'        
      }
    ]
  }

]

url = "https://target-sandbox.my.com"
login = "buck2192@gmail.com"
password = "123qweasd"

MyTargetPadsCreator.new(
  url: url,
  login: login,
  password: password,
  pads_to_create: pads_to_create
).start
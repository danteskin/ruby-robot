#install gems
bundle install

#if you have problem with install watir

gem install watir


#how to install phantomJS:
install for linux

-download archive with offical site http://phantomjs.org/download.html

-replace archive in usr/local/share

-add phantomJS to PATH
```
#!linux
cd /usr/local/share
sudo tar xjf phantomjs-2.1.1-linux-x86_64.tar.bz2

sudo ln -s /usr/local/share/phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/share/phantomjs
sudo ln -s /usr/local/share/phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin/phantomjs
sudo ln -s /usr/local/share/phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/bin/phantomjs

```

#auth setting:
open the file and at the bottom of the file please set variables for auth "url page", "your login", "your pass"

#create pads:
for create pad in array "pads to create" add or update object as in the example, and enter name,description and link for your app

#start robot
run start.rb "bundle exec ruby start.rb"

#start test
bundle exec rspec test.rb

#main file - script.rb
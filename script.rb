require 'rubygems'
#! /usr/bin/env ruby

#
require 'bundler/setup'

require "nokogiri"
require "open-uri"
require "watir"
# color text
require "colorize"

# for automating interaction with websites 
# require 'mechanize'

# test.rb
require 'pry'



module Browser
  def browser
    
    
    @browser ||= Watir::Browser.new :phantomjs

  end

  def visit_page(url)
    browser.goto url
  end
end

class MyTargetPadsCreator

  include Browser

  attr_reader :url, :login, :password, :pads_to_create

  def initialize(url:, login:, password:, pads_to_create:)
    @url = url
    @login = login
    @password = password
    @pads_to_create = pads_to_create
  end

  def start
    visit_page @url
    authorize
    @pads_to_create.each { |pad_data| create_pad!(pad_data) }
    # create_pads!(@pads_to_create)
  end

  def authorize
    # puts "step1 open popup"
    #open popup
    span_login = browser.span class: 'ph-button__inner_profilemenu_signin'
    span_login.wait_until_present(timeout: 90)
    span_login.click
    # puts "step2 wait auth form and find fields and button submit"
    #wait auth form and find fields and button submit
    auth_popup = browser.div class: 'auth-popup'
    auth_popup.wait_until_present
    auth_login_field = auth_popup.text_field(name: "login")
    auth_password_field = auth_popup.text_field(name: "password")
    auth_submit_button = auth_popup.button class: 'button_submit'
    # puts "step3 set value in fields of auth_form"
    #set value in fields of auth_form
    auth_login_field.set @login
    auth_password_field.set @password
    #submit auth_form
    auth_submit_button.click
    puts "authorization end".green
    # puts(browser.url.to_s.downcase == "https://target-sandbox.my.com/pad_groups/" ? "authorization success, auth page title is: '#{browser.title}', url: '#{browser.url}'".green : "WARNING: failed because Page title: '#{browser.title}' , url: '#{browser.url}', page wrong or page title was changed, or check your login or password".yellow ) 
  end

  def create_pad!(attributes)    
    #get description app link,count of Ad Units,and Ad Unit
    description = attributes[:description]
    app_link = attributes[:app_link]
    ad_units = attributes[:ad_units]
    ad_units_count = ad_units.length    

    create_pad_with_first_ad_unit(
      description: description,
      app_link: app_link,
      ad_units: ad_units
    )
    
    create_additional_ad_units(ad_units: ad_units) if ad_units_count > 1
    
  end
  
  def create_pad_with_first_ad_unit(description:, app_link:, ad_units:)
    #page of my companies
    puts "trying to find button create for company"
    browser.goto("#{@url}/pad_groups/")
    company_button_create = browser.link class: 'pad-groups-control-panel__button_create'
    company_button_create.wait_until_present(timeout: 100)
    company_button_create.click
    
    #define var for fields and button
    create_pad_description_field = browser.text_field class: "pad-setting__description__input"
    create_pad_url_field = browser.text_field class: "pad-setting__url__input"
    
    #..waiting and setting values
    create_pad_description_field.wait_until_present.set description
    create_pad_url_field.wait_until_present.set app_link
    
    create_pad_adv_name.wait_until_present(timeout: 90).set ad_units.first[:name]
    #submit create pads and first ad unit
    puts "trying to find the submit button after fields set values"
    create_pad_submit_button = browser.span class: "main-button-new"
    create_pad_submit_button.wait_until_present
    puts "click submit to create pad with default checkbox"
    create_pad_submit_button.click
    #return page of the group pads
    puts "wait graths and return page 'my pad' to url #{@url}/pad_groups/"
    company_button_create.wait_until_present(timeout: 100)
    #check new pad response from site
    browser.goto("#{@url}/api/v2/pad_groups.json")
    response_pad_groups = browser.text
    pad_groups_json = JSON.parse(response_pad_groups)
    # pp pad_groups_json.last
    if pad_groups_json.last["description"] == description
      puts "success test last id".green
    else
      puts "something wrong".red
    end
  end  

  def create_additional_ad_units(ad_units:)
    puts "add additional units more".yellow

    back_url = browser.url
    browser.goto("#{@url}/api/v2/pad_groups.json")
    response_pad_groups = browser.text    
    pad_groups_json = JSON.parse(response_pad_groups)
    create_pad_id = pad_groups_json.last["id"]    
    
    ad_units[1..-1].each do |ad_unit|
      name = ad_unit[:name]
      browser.goto("#{@url}/pad_groups/#{create_pad_id}/create/")
      puts "create page with pad id #{create_pad_id}".green

      puts "wait fields and set values"
      # create_pad_description_field.wait_until_present.set description
      create_pad_adv_name.wait_until_present.set name
      puts "submit button for ad_unit"
      ad_unit_create_submit.wait_until_present(timeout: 100).click
      
      #wait loading page with success response
      
      ad_unit_button_for_create.wait_until_present(timeout: 100)
      
      browser.goto("#{@url}/api/v2/pad_groups/#{create_pad_id}.json")
      response_pad_with_id = browser.text
      # binding.pry
      pad_with_id_json = JSON.parse(response_pad_with_id)
      unit_id = pad_with_id_json['pads'].last['id']
      pad_with_id_json['pads'].last['description']
      if pad_with_id_json['pads'].last['description'] == name
        puts "Unit #{name} add with id=#{unit_id}".green
      else
        puts "something wrong".yellow
      end

      # pp pad_with_id_json
      # binding.pry
      # puts browser.url.green
      
      puts "success add Ad unit ".green  
    end

    # pp pad_groups_json.last
    puts "all unit add".green

    browser.goto("#{@url}/pad_groups/")
  end

  

  private

  def create_pad_adv_name
    create_pad_adv_name = browser.text_field class: "adv-block-form__row__input"
  end

  def ad_unit_create_submit
    #button for save and submit adunit
    ad_unit_create_submit = browser.span class: "create-pad-page__save-button"
  end

  def ad_unit_button_for_create
    #button (link) for create ad unit 
    ad_unit_button_for_create = browser.link class: "pads-control-panel__button pads-control-panel__button_create"
  end

  
end


